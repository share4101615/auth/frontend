"use client";
import { authTheme } from "@/configs/theme/themes";
import { validateAccessToken } from "@/features/auth/lib/tokenHandlers";
import { CssBaseline } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { useRouter } from "next/navigation";
import { useLayoutEffect } from "react";

type Props = {
	children: React.ReactNode;
};
export default function AuthLayout({ children }: Readonly<Props>) {
	const router = useRouter();

	useLayoutEffect(() => {
		const isAuthed = validateAccessToken();
		if (isAuthed) {
			router.replace("/viewer");
		}
	}, [router]);

	return (
		<ThemeProvider theme={authTheme}>
			<CssBaseline />
			{children}
		</ThemeProvider>
	);
}
