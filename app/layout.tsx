"use client";
import React from "react";
import { ThemeProvider } from "@mui/material/styles";
import { AppRouterCacheProvider } from "@mui/material-nextjs/v14-appRouter";
import { CssBaseline, useMediaQuery } from "@mui/material";
import { QueryClientProvider } from "@tanstack/react-query";

import { darkTheme, lightTheme, queryClient } from "@/configs";
import { useSettingsStore } from "@/stores";

type Props = {
	children: React.ReactNode;
};

export default function RootLayout({ children }: Readonly<Props>) {
	const { theme } = useSettingsStore();
	const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)")
		? "dark"
		: "light";
	const chosenTheme = theme ?? prefersDarkMode;

	return (
		<html lang="ru">
			<head>
				<meta charSet="utf-8" />
				<meta
					name="viewport"
					content="minimum-scale=1, initial-scale=1, width=device-width"
				/>
				<title>SIA редактор</title>
			</head>
			<body>
				<AppRouterCacheProvider>
					<QueryClientProvider client={queryClient}>
						<ThemeProvider
							theme={chosenTheme === "dark" ? darkTheme : lightTheme}
						>
							<CssBaseline />
							{children}
						</ThemeProvider>
					</QueryClientProvider>
				</AppRouterCacheProvider>
			</body>
		</html>
	);
}
