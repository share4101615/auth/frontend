"use client";
import { useAuth } from "@/features";

type Props = {
	children: React.ReactNode;
};

export default function ViewerLayout({ children }: Readonly<Props>) {
	const isAuthed = useAuth();

	return isAuthed ? <>{children}</> : null;
}
