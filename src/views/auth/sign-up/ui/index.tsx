"use client";
import { Container, Stack, useMediaQuery, useTheme } from "@mui/material";
import { Text } from "./Text";
import { FullSignUpForm } from "./FullSignUpForm";
import { BackButton } from "@/shared/components";

export const SignUpPage = () => {
	const theme = useTheme();
	const bigSize = useMediaQuery(theme.breakpoints.up("md"));

	return (
		<Container maxWidth="md">
			<BackButton />
			<Stack
				height={"100dvh"}
				direction="row"
				alignItems="center"
				justifyContent="space-around"
			>
				<FullSignUpForm />
				{bigSize && <Text />}
			</Stack>
		</Container>
	);
};
