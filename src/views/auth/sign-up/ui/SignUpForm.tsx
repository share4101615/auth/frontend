"use client";
import { FormControl, FormHelperText, Stack, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { useHandleSignUpForm } from "../hooks/useHandleSignUpForm";
import { useSignUpFetch } from "../hooks/useSignUpFetch";
import { validateEmail, validatePassword } from "@/features";
import { PasswordField } from "@/shared/components";
import React from "react";

export const SignUpForm = () => {
	const { mutate, isPending, isError } = useSignUpFetch();
	const { Field, handleSubmit } = useHandleSignUpForm({ mutate });

	return (
		<form
			onSubmit={(e) => {
				e.preventDefault();
				void handleSubmit();
			}}
		>
			<FormControl sx={{ width: "100%" }}>
				<Stack spacing={2}>
					<Field
						name="firstName"
						children={(field) => (
							<TextField
								label="Имя"
								placeholder="Введите имя"
								autoComplete="given-name"
								autoFocus={true}
								fullWidth={true}
								defaultValue={field.state.value}
								onChange={(e) => field.handleChange(e.target.value)}
								error={Boolean(field.state.meta.errors.length)}
								helperText={field.state.meta.errors[0] ?? ""}
							/>
						)}
					/>
					<Field
						name="lastName"
						children={(field) => (
							<TextField
								label="Фамилия"
								placeholder="Фамилия"
								autoComplete="family-name"
								fullWidth={true}
								defaultValue={field.state.value}
								onChange={(e) => field.handleChange(e.target.value)}
								error={Boolean(field.state.meta.errors.length)}
								helperText={field.state.meta.errors[0] ?? ""}
							/>
						)}
					/>
					<Field
						name="email"
						children={(field) => (
							<TextField
								label="Почта"
								placeholder="E-mail"
								autoComplete="email"
								fullWidth={true}
								defaultValue={field.state.value}
								onChange={(e) => field.handleChange(e.target.value)}
								error={Boolean(field.state.meta.errors.length)}
								helperText={field.state.meta.errors[0] ?? ""}
							/>
						)}
						validators={validateEmail}
					/>
					<Field
						name="password"
						children={(field) => <PasswordField field={field} />}
						validators={validatePassword}
					/>
					<LoadingButton type="submit" variant="contained" loading={isPending}>
						Зарегистрироваться
					</LoadingButton>
					{!isError && <div style={{ margin: "1rem 0" }}></div>}
					{isError && (
						<FormHelperText error>
							Такой пользователь уже существует
						</FormHelperText>
					)}
				</Stack>
			</FormControl>
		</form>
	);
};
