import { Stack, Typography } from "@mui/material";
import { SignUpForm } from "./SignUpForm";
import { Link } from "@/shared/components";

export const FullSignUpForm = () => {
	return (
		<Stack spacing={3} direction="column" justifyContent="center" width="370px">
			<Typography variant="h2">Регистрация</Typography>
			<SignUpForm />
			<Stack direction="row" justifyContent="space-between">
				<Typography variant="body1">Вы уже зарегистрированы?</Typography>
				<Link href="/auth/sign-in" variant="underline">
					Войдите
				</Link>
			</Stack>
		</Stack>
	);
};
