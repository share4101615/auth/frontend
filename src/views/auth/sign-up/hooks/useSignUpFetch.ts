import { AUTH_KEY } from "@/configs";
import { clearTokens, setupTokens } from "@/features";
import { fetchSignUp } from "@/shared/api";
import { ISignUp } from "@nbelous/share-auth";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";

export const useSignUpFetch = () => {
	const router = useRouter();

	return useMutation({
		mutationKey: [AUTH_KEY],
		mutationFn: (body: ISignUp) => fetchSignUp(body),
		onSuccess: (data) => {
			setupTokens(data);
			router.replace("/viewer");
		},
		onError: async () => {
			clearTokens();
		},
	});
};
