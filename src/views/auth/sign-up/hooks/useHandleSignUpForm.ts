import { ISignUp, ITokens } from "@nbelous/share-auth";
import { useForm } from "@tanstack/react-form";
import { UseMutateFunction } from "@tanstack/react-query";

type Params = {
	mutate: UseMutateFunction<ITokens, unknown, ISignUp, unknown>;
};

export const useHandleSignUpForm = ({ mutate }: Params) =>
	useForm<ISignUp>({
		defaultValues: {
			email: "",
			password: "",
			firstName: "",
			lastName: "",
		},
		onSubmit: async ({ value }) => {
			mutate(value);
		},
	});
