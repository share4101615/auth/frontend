import { AUTH_KEY } from "@/configs";
import { clearTokens, setupTokens } from "@/features";
import { fetchSignIn } from "@/shared/api";
import { ISignIn } from "@nbelous/share-auth";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";

export const useSignInFetch = () => {
	const router = useRouter();

	return useMutation({
		mutationKey: [AUTH_KEY],
		mutationFn: (body: ISignIn) => fetchSignIn(body),
		onSuccess: (data) => {
			setupTokens(data);
			router.replace("/viewer");
		},
		onError: async (error) => {
			clearTokens();
			return "Неверный логин или пароль";
		},
	});
};
