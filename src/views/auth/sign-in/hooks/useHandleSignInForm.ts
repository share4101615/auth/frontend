import { ISignIn, ITokens } from "@nbelous/share-auth";
import { useForm } from "@tanstack/react-form";
import { UseMutateFunction } from "@tanstack/react-query";

type Params = {
	mutate: UseMutateFunction<ITokens, unknown, ISignIn, unknown>;
};

export const useHandleSignInForm = ({ mutate }: Params) =>
	useForm<ISignIn>({
		defaultValues: {
			email: "",
			password: "",
		},
		onSubmit: async ({ value }) => {
			mutate(value);
		},
	});
