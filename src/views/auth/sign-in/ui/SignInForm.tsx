"use client";
import {
	Checkbox,
	FormControl,
	FormControlLabel,
	FormHelperText,
	Stack,
	TextField,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { useHandleSignInForm } from "../hooks/useHandleSignInForm";
import { useSignInFetch } from "../hooks/useSignInFetch";
import { validateEmail, validatePassword } from "@/features";
import { PasswordField } from "@/shared/components";

export const SignInForm = () => {
	const { mutate, isPending, isError } = useSignInFetch();
	const { Field, handleSubmit } = useHandleSignInForm({ mutate });

	return (
		<form
			onSubmit={(e) => {
				e.preventDefault();
				void handleSubmit();
			}}
		>
			<FormControl sx={{ width: "100%" }}>
				<Stack spacing={1.25}>
					<Field
						name="email"
						children={(field) => (
							<TextField
								autoFocus
								autoComplete="on"
								label="E-mail"
								placeholder="E-mail"
								fullWidth={true}
								value={field.state.value}
								onChange={(e) => {
									field.handleChange(e.target.value);
								}}
								error={Boolean(field.state.meta.errors.length > 0)}
								helperText={field.state.meta.errors[0]}
							/>
						)}
						validators={validateEmail}
					/>
					<Field
						name="password"
						children={(field) => <PasswordField field={field} />}
						validators={validatePassword}
					/>
					<FormControlLabel
						sx={{ mt: 0 }}
						control={<Checkbox />}
						label="Запомнить меня"
					/>
					<LoadingButton type="submit" variant="contained" loading={isPending}>
						Войти
					</LoadingButton>
					{!isError && <div style={{ margin: "1rem 0" }}></div>}
					{isError && (
						<FormHelperText error>Логин или пароль неверны</FormHelperText>
					)}
				</Stack>
			</FormControl>
		</form>
	);
};
