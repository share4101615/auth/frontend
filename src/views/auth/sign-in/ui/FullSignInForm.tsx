import { Stack, Typography } from "@mui/material";
import { SignInForm as Form } from "./SignInForm";
import { Link } from "@/shared/components";

export const FullSignInForm = () => {
	return (
		<Stack direction="column">
			<Stack
				spacing={3}
				direction="column"
				justifyContent="center"
				width="370px"
			>
				<Typography variant="h2">Войдите в ЛК</Typography>
				<Form />
			</Stack>
			<Stack direction="row" justifyContent="space-between">
				<Typography>Забыли пароль? </Typography>
				<Link href="/auth/recovery-password" variant="underline">
					Восстановить
				</Link>
			</Stack>
			<Stack direction="row" justifyContent="space-between">
				<Typography>У Вас еще нет аккаунта? </Typography>
				<Link href="/auth/sign-up" variant="underline">
					Создать
				</Link>
			</Stack>
		</Stack>
	);
};
