"use client";
import { Container, Stack, useMediaQuery, useTheme } from "@mui/material";
import { Text } from "./Text";
import { FullSignInForm } from "./FullSignInForm";

export const SignInPage = () => {
	const theme = useTheme();
	const bigSize = useMediaQuery(theme.breakpoints.up("md"));

	return (
		<Container maxWidth="md">
			<Stack
				height={"100dvh"}
				direction="row"
				alignItems="center"
				justifyContent="space-around"
			>
				<FullSignInForm />
				{bigSize && <Text />}
			</Stack>
		</Container>
	);
};
