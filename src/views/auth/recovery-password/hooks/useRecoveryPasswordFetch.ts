import { AUTH_KEY } from "@/configs";
import { fetchRecoveryPassword } from "@/shared/api";
import { IRecoveryPassword } from "@nbelous/share-auth";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";

export const useRecoveryPasswordFetch = () => {
	const router = useRouter();

	return useMutation({
		mutationKey: [AUTH_KEY],
		mutationFn: (body: IRecoveryPassword) => fetchRecoveryPassword(body),
		onSuccess: () => {
			router.replace("/auth/sign-in");
		},
	});
};
