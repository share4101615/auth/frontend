import { IRecoveryPassword } from "@nbelous/share-auth";
import { useForm } from "@tanstack/react-form";
import { UseMutateFunction } from "@tanstack/react-query";

type Params = {
	mutate: UseMutateFunction<unknown, unknown, IRecoveryPassword, unknown>;
};

export const useHandleRecoveryPasswordForm = ({ mutate }: Params) =>
	useForm<IRecoveryPassword>({
		defaultValues: {
			email: "",
		},
		onSubmit: async ({ value }) => {
			mutate(value);
		},
	});
