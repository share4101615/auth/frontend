"use client";
import {
	Container,
	Stack,
	Typography,
	useMediaQuery,
	useTheme,
} from "@mui/material";
import { RecoveryForm } from "./RecoveryForm";
import { DotsIcon } from "../images";
import { BackButton } from "@/shared/components";

export const RecoveryPasswordPage = () => {
	const theme = useTheme();
	const bigSize = useMediaQuery(theme.breakpoints.up("sm"));

	return (
		<Container maxWidth="lg">
			<BackButton />
			<Stack
				direction="row"
				spacing={"4rem"}
				style={{
					justifyContent: "center",
					height: "100vh",
					alignItems: "center",
				}}
			>
				<Stack direction="column">
					<Typography variant="h2" align="left">
						Восстановление пароля
					</Typography>
					<RecoveryForm />
				</Stack>
				{bigSize && (
					<div style={{ marginTop: "2rem" }}>
						<DotsIcon />
					</div>
				)}
			</Stack>
		</Container>
	);
};
