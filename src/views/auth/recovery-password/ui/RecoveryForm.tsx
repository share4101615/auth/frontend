import { validateEmail } from "@/features";
import { Stack, TextField } from "@mui/material";
import { useRecoveryPasswordFetch } from "../hooks/useRecoveryPasswordFetch";
import { useHandleRecoveryPasswordForm } from "../hooks/useHandleRecoveryPasswordForm";
import { LoadingButton } from "@mui/lab";

export const RecoveryForm = () => {
	const { mutate, isPending } = useRecoveryPasswordFetch();
	const { Field, handleSubmit } = useHandleRecoveryPasswordForm({ mutate });

	return (
		<div style={{ marginTop: "1rem" }}>
			<form
				onSubmit={(e) => {
					e.preventDefault();
					e.stopPropagation();
					void handleSubmit();
				}}
			>
				<Stack spacing={"1rem"} direction="column">
					<Field
						name="email"
						children={(field) => (
							<TextField
								label="E-mail"
								placeholder="Введите адрес электронной почты"
								autoFocus={true}
								autoComplete="email"
								fullWidth={true}
								defaultValue={field.state.value}
								onChange={(e) => field.handleChange(e.target.value)}
								error={Boolean(field.state.meta.errors.length)}
								helperText={field.state.meta.errors[0] || ""}
							/>
						)}
						validators={validateEmail}
					/>
					<LoadingButton type="submit" variant="contained" loading={isPending}>
						Восстановить
					</LoadingButton>
				</Stack>
			</form>
		</div>
	);
};
