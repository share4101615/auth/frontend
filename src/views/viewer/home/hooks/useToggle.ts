import React from "react";

export const useToggle = () => {
	const [open, setOpen] = React.useState<boolean>(true);

	const onToggle = React.useCallback(() => {
		setOpen((prev) => !prev);
	}, []);

	return { open, onToggle };
};
