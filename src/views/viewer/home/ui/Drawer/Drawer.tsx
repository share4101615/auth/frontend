import React from "react";
import { IconButton, Drawer as MuiDrawer, Stack } from "@mui/material";
import { ClosePanelIcon } from "@/shared/icons";
import { SxProps } from "@mui/system";

import { ResizeElement } from "./ResizeElement";
import { useToggle } from "../../hooks/useToggle";
import { UserInfo } from "./UserInfo/UserInfo";
import { useGetAccount} from "@/features";

const drawerSx = (width: number): SxProps => ({
	width: width,
	flexShrink: 0,
	[".MuiDrawer-paper"]: {
		width: width,
		boxSizing: "border-box",
	},
});

export const Drawer = () => {
	const [width, setWidth] = React.useState<number>(240);
	const { open, onToggle } = useToggle();
	const { data: accountData } = useGetAccount();

	if (!open) {
		return (
			<Stack
				sx={{
					height: "100dvh",
				}}
			>
				<IconButton
					onClick={onToggle}
					color="primary"
					sx={{ mr: 1, ml: 0.5, mt: 0.5 }}
				>
					<ClosePanelIcon />
				</IconButton>
			</Stack>
		);
	}

	return (
		<Stack direction="row">
			<MuiDrawer
				open={open}
				anchor="left"
				onClose={onToggle}
				variant="persistent"
				sx={drawerSx(width)}
			>
				<UserInfo data={accountData}>
					<IconButton onClick={onToggle} color="primary" sx={{ mr: 1 }}>
						<ClosePanelIcon />
					</IconButton>
				</UserInfo>
			</MuiDrawer>
			<ResizeElement setWidth={setWidth} />
		</Stack>
	);
};
