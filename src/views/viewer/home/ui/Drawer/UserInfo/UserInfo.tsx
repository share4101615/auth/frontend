import { Stack, Typography } from "@mui/material";
import { IGetAccount } from "@nbelous/share-auth";
import React from "react";

type Props = {
	children: React.ReactNode;
	readonly data?: IGetAccount;
};

export const UserInfo = ({ children, data }: Props) => {
	if (!data) {
		return null;
	}

	return (
		<Stack direction="row" p={0.5}>
			<div>{children}</div>
			<Stack direction="column">
				<Typography variant="h5" component="p" noWrap>
					{data.firstName ?? data.email ?? "Пользователь"}
				</Typography>
			</Stack>
		</Stack>
	);
};
