import React from "react";

type Props = {
	setWidth: (width: number) => void;
};

export const ResizeElement = ({ setWidth }: Props) => {
	const startResizing = React.useCallback(() => {
		// Функция для изменения размера
		const handleMouseMove = (mouseMoveEvent: MouseEvent) => {
			const newWidth = mouseMoveEvent.clientX;
			if (newWidth > 240 && newWidth < 800) {
				setWidth(newWidth);
			}
		};

		// Остановить изменение размера
		const stopResizing = () => {
			window.removeEventListener("mousemove", handleMouseMove);
			window.removeEventListener("mouseup", stopResizing);
		};

		window.addEventListener("mousemove", handleMouseMove);
		window.addEventListener("mouseup", stopResizing);
	}, []);
	return (
		<div
			role="none"
			style={{
				padding: "4px",
				cursor: "col-resize",
				height: "100dvh",
			}}
			onMouseDown={startResizing}
		></div>
	);
};
