"use client";

import { useLogout } from "@/features";
import { Button, Container, Paper, Stack } from "@mui/material";
import { Drawer } from "./Drawer/Drawer";

export const ViewerHomePage = () => {
	const { handleLogout } = useLogout();
	return (
		<Stack direction="row">
			<Drawer />
			<div
				style={{
					width: "100%",
				}}
			>
				<Container maxWidth="lg">
					<Paper square>
						Главная страница - лендинг
						<Button onClick={handleLogout}>Выйти</Button>
					</Paper>
				</Container>
			</div>
		</Stack>
	);
};
