import { useRouter } from "next/navigation";
import { useLayoutEffect, useState } from "react";
import { clearTokens, validateAccessToken } from "../lib/tokenHandlers";

export const useAuth = () => {
	const router = useRouter();
	const [auth, setAuth] = useState(false);

	useLayoutEffect(() => {
		let isAuthed = validateAccessToken();

		if (!isAuthed) {
			setAuth(false);
			clearTokens();
			router.push("/auth/sign-in");
		} else {
			setAuth(true);
		}
	}, [router, setAuth]);

	return Boolean(auth);
};
