"use client";
import { useRouter } from "next/navigation";
import React from "react";
import { clearTokens, validateAccessToken } from "../lib/tokenHandlers";

export const useLogout = () => {
	const router = useRouter();

	const handleLogout = React.useCallback(() => {
		clearTokens();
		router.push("/auth/sign-in");
	}, [router]);

	return { handleLogout };
};
