import { ITokens } from "@nbelous/share-auth";

const ACCESS_TOKEN_KEY = "accessToken";
const REFRESH_TOKEN_KEY = "refreshToken";

export const setupTokens = (data: ITokens) => {
	localStorage.setItem(ACCESS_TOKEN_KEY, data.accessToken);
	localStorage.setItem(REFRESH_TOKEN_KEY, data.refreshToken);
};

export const getAccessToken = () => {
	return localStorage.getItem(ACCESS_TOKEN_KEY);
};

export const getRefreshToken = () => {
	return localStorage.getItem(REFRESH_TOKEN_KEY);
};

export const clearTokens = () => {
	localStorage.removeItem(ACCESS_TOKEN_KEY);
	localStorage.removeItem(REFRESH_TOKEN_KEY);
};

type EncodedToken = {
	sub: number;
	iat: number;
	exp: number;
	aud: string;
	iss: string;
};

export const parseJWTTokens = (token: string): EncodedToken | null => {
	if (!token) {
		return null;
	}
	const base64Url = token.split(".")[1];
	const base64 = base64Url.replace("-", "+").replace("_", "/");
	return JSON.parse(window.atob(base64));
};

export const isTokenExpired = (token: Pick<EncodedToken, "exp">): boolean => {
	const currentTime = Math.floor(Date.now() / 1000);
	const diff = token.exp - currentTime;

	return diff > 0 ? false : true;
};

// Получает токен и возвращает true, если он не истек
export const validateAccessToken = (): boolean => {
	let valid = false;
	const token = getAccessToken();
	if (token) {
		const parsedToken = parseJWTTokens(token);
		if (parsedToken?.exp) {
			valid = !isTokenExpired({ exp: parsedToken.exp });
		}
	}

	return valid;
};
