import { z } from "zod";

// TODO: Использовать типы zod
type ValidatorConfig = {
	// onChangeAsync: (param: { value: string}) => Promise<string>;
	// onChangeAsyncDebounceMs: number;
	onBlurAsync: (param: { value: string }) => Promise<string>;
	onBlurAsyncDebounceMs: number;
};

const validator = (cb: (param: string) => void): ValidatorConfig => ({
	onBlurAsync: async (e): Promise<string> => {
		try {
			cb(e.value);
			return "";
		} catch (error) {
			if (error instanceof z.ZodError) {
				return error.errors[0].message;
			}
			return "Ошибка валидации";
		}
	},
	onBlurAsyncDebounceMs: 500,
});

const emailSchema = z.string().email({ message: "Неверный формат почты" });
const passwordSchema = z
	.string()
	.min(6, { message: "Пароль должен быть не менее 6 символов" })
	.max(32, { message: "Пароль не может быть более 32 символов" });

export const validateEmail = validator((e) => emailSchema.parse(e));
export const validatePassword = validator((e) => passwordSchema.parse(e));
