import { useQuery } from "@tanstack/react-query";

import { ACCOUNTS_KEY } from "@/configs";
import { fetchAccount } from "@/shared/api";

export const useGetAccount = () =>
	useQuery({
		queryKey: [ACCOUNTS_KEY],
		queryFn: () => fetchAccount(),
	});
