import { FieldApi } from "@tanstack/react-form";
import {
	FormControl,
	FormHelperText,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
} from "@mui/material";

import React from "react";
import { EyeIcon, EyeOffIcon } from "@/shared/icons";

interface Props {
	field: FieldApi<any, any>;
}
export const PasswordField = ({ field }: Props) => {
	const [showPassword, setShowPassword] = React.useState(false);
	const handleClickShowPassword = () => setShowPassword((show) => !show);
	const handleMouseDownPassword = (
		event: React.MouseEvent<HTMLButtonElement>
	) => {
		event.preventDefault();
	};
	const isError = Boolean(field.state.meta.errors.length);

	return (
		<FormControl variant="outlined" fullWidth>
			<InputLabel
				shrink
				htmlFor="password"
				error={isError}
				sx={{
					backgroundColor: "background.default",
					padding: "0 0.3rem",
				}}
			>
				Пароль
			</InputLabel>
			<OutlinedInput
				placeholder="Введите пароль"
				id="password"
				error={isError}
				onChange={(e) => field.handleChange(e.target.value)}
				type={showPassword ? "text" : "password"}
				endAdornment={
					<InputAdornment position="end">
						<IconButton
							onClick={handleClickShowPassword}
							onMouseDown={handleMouseDownPassword}
							edge="end"
						>
							{showPassword ? <EyeOffIcon /> : <EyeIcon />}
						</IconButton>
					</InputAdornment>
				}
				label="Пароль"
				autoComplete="off"
			/>
			<FormHelperText error={isError}>
				{field.state.meta.errors[0] ?? ""}
			</FormHelperText>
		</FormControl>
	);
};
