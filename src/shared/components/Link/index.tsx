"use client";
import { Link as MuiLink, useTheme } from "@mui/material";
import { RouteType } from "next/dist/lib/load-custom-routes";
import NextLink, { LinkProps } from "next/link";
import { CSSProperties } from "react";

const defaultLinkStyle: CSSProperties = {
	textDecoration: "none",
	cursor: "pointer",
};

type Props = LinkProps<RouteType> & {
	variant?: "default" | "underline";
};

export const Link = ({ children, href, variant = "default" }: Props) => {
	const theme = useTheme();

	if (variant === "underline") {
		const underlineStyles: CSSProperties = {
			textDecoration: "underline",
			color: theme.palette.primary.main,
			cursor: "pointer",
		};

		return (
			<NextLink href={href} style={underlineStyles}>
				<MuiLink sx={underlineStyles} component="div">
					{children}
				</MuiLink>
			</NextLink>
		);
	}

	return (
		<NextLink href={href} style={defaultLinkStyle}>
			<MuiLink sx={defaultLinkStyle} component="div">
				{children}
			</MuiLink>
		</NextLink>
	);
};
