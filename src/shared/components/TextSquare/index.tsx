"use client";
import { Typography } from "@mui/material";
import { ReactNode } from "react";
import { CursorIcon, CursorMagicWandIcon } from "@/shared/icons";
import { LeftBottomSquare, LeftTopSquare, RightBottomSquare, RightTopSquare, Circle, cursorStyle, PromptWrapper } from "./DecorativeElements";

type Props = {
	children: ReactNode;
	cursorType?: "default" | "magicWand";
	circleType?: "one" | "two";
};


export const TextSquare = ({ children, cursorType = "default", circleType = "one" }: Props) => {
	return (
		<PromptWrapper>
			<LeftTopSquare />
			<LeftBottomSquare />
			<RightTopSquare />
			<RightBottomSquare />
			{circleType === "one" ? <Circle /> : null}
			{cursorType === "default" ? <CursorIcon style={cursorStyle} /> : <CursorMagicWandIcon style={cursorStyle} />}
			<Typography
				style={{
					fontWeight: 700,
					fontSize: "1.5rem",
					padding: "1rem",
				}}
			>
				{children}
			</Typography>
		</PromptWrapper>
	);
};
