import { styled } from "@mui/material";

const Square = styled("div")(({ theme }) => ({
	position: "absolute",
	width: "9px",
	height: "9px",
	backgroundColor: theme.palette.background.default,
	border: `1px solid ${theme.palette.primary.contrastText}`,
}));

export const LeftTopSquare = styled(Square)(({ theme }) => ({
	backgroundColor: `${theme.palette.primary.main}`,
	top: "-4.5px",
	left: "-4.5px",
}));

export const LeftBottomSquare = styled(Square)(() => ({
	bottom: "-4.5px",
	left: "-4.5px",
}));

export const RightTopSquare = styled(Square)(() => ({
	top: "-4.5px",
	right: "-4.5px",
}));

export const RightBottomSquare = styled(Square)(() => ({
	bottom: "-4.5px",
	right: "-4.5px",
}));

export const Circle = styled("div")(({ theme }) => ({
	position: "absolute",
	width: "112px",
	height: "112px",
	backgroundColor: `${theme.palette.primary.main}`,
	borderRadius: "66px",
	zIndex: "-1",
	top: "-40px",
	left: "-40px",
}));

export const cursorStyle: React.CSSProperties = {
	position: "absolute",
	bottom: "-60px",
	right: "50px",
};

export const PromptWrapper = styled("div")(({ theme }) => ({
	position: "relative",
	maxWidth: "300px",
	border: `1px solid ${theme.palette.primary.contrastText}`,
}));
