"use client";
import { IconButton, Stack, Typography } from "@mui/material";

import { BackArrowIcon } from "@/shared/icons";

export const BackButton = () => {
	const handleBackClick = () => {
		window.history.back();
	};

	return (
		<Stack
			direction="row"
			style={{ position: "absolute", top: "2%", left: "2%" }}
		>
			<IconButton color="primary" onClick={handleBackClick}>
				<BackArrowIcon />
			</IconButton>
			<Typography style={{ paddingLeft: 8, paddingTop: 4 }}>
				Вернуться назад
			</Typography>
		</Stack>
	);
};
