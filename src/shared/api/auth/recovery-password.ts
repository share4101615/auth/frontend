import { backendRequestInstance } from "@/configs/api/backend";
import { IRecoveryPassword } from "@nbelous/share-auth";

export const fetchRecoveryPassword = (
	body: IRecoveryPassword
): Promise<unknown> => {
	if (body.email === "") {
		throw new Error("Email обязателен");
	}

	return backendRequestInstance.post("auth/recovery-password", {
		json: body,
	});
};
