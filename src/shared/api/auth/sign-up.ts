import { backendRequestInstance } from "@/configs/api/backend";
import { ISignUp, ITokens } from "@nbelous/share-auth";

export const fetchSignUp = (body: ISignUp): Promise<ITokens> => {
	if (body.email === "" || body.password === "") {
		throw new Error("Логин и пароль обязательны");
	}

	return backendRequestInstance
		.post("auth/sign-up", {
			json: body,
		})
		.json<ITokens>();
};
