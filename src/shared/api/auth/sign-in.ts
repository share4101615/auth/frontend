import { backendRequestInstance } from "@/configs/api/backend";
import { ISignIn, ITokens } from "@nbelous/share-auth";

export const fetchSignIn = (body: ISignIn): Promise<ITokens> => {
	if (body.email === "" || body.password === "") {
		throw new Error("Логин и пароль обязательны");
	}

	return backendRequestInstance
		.post("auth/sign-in", {
			json: body,
		})
		.json<ITokens>();
};
