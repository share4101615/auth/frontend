import { backendRequestInstance } from "@/configs/api/backend";
import { IGetAccount } from "@nbelous/share-auth";

export const fetchAccount = (): Promise<IGetAccount> => {
	return backendRequestInstance.get("accounts").json();
};
