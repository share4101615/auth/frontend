import { create } from "zustand";

interface ISettingsStore {
	theme: null | "light" | "dark";
	setTheme: (theme: "light" | "dark") => void;
}
export const useSettingsStore = create<ISettingsStore>((set) => ({
    theme: null,
	setTheme: (theme) => set({ theme }),
}));