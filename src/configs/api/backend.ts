import { getAccessToken } from "@/features";
import { QueryClient } from "@tanstack/react-query";
import ky from "ky";

export const backendRequestInstance = ky.create({
	// prefixUrl: "http://localhost:3005/api/",
	prefixUrl: process.env.NEXT_PUBLIC_BACKEND_URL,
	hooks: {
		beforeRequest: [
			(request) => {
				const token = getAccessToken();
				if (token) {
					// Добавляем заголовок Authorization, если токен доступен
					request.headers.set("Authorization", `Bearer ${token}`);
				}
			},
		],
	},
});

export const queryClient = new QueryClient({
	defaultOptions: {
		queries: {},
		mutations: {},
	},
});
