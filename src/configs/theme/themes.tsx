"use client";
import { ThemeOptions, createTheme } from "@mui/material";
import { merge } from "lodash";

import { commonStyles } from "./commonStyles";
import { authStyles } from "./authStyles";
import { darkStyles } from "./darkStyles";
import { ligtStyles } from "./ligtStyles";

const authThemeStyle: ThemeOptions = merge(authStyles, commonStyles);
const darkThemeStyle: ThemeOptions = merge(darkStyles, commonStyles);
const lightThemeStyle: ThemeOptions = merge(ligtStyles, commonStyles);

export const authTheme = createTheme(authThemeStyle);
export const darkTheme = createTheme(darkThemeStyle);
export const lightTheme = createTheme(lightThemeStyle);
