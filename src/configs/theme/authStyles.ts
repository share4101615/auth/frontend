import { ThemeOptions } from "@mui/material";

export const authStyles: ThemeOptions = {
	palette: {
		mode: "dark",
		primary: {
			main: "#0066CC",
		},
		background: {
			default: "#000000",
		},
	},
};
