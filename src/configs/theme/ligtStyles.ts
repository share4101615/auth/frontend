import { ThemeOptions } from "@mui/material";

export const ligtStyles: ThemeOptions = {
	palette: {
		action: {
			selected: "#0066CC",
		},
		primary: {
			main: "#333333",
		},
		secondary: {
			light: "#E5E5E5",
			main: "#999999",
		},
		info: {
			main: "#0066CC",
		},
		background: {
			default: "#FFFFFF",
		},
	},
};
