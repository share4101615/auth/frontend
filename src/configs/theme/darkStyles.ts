import { ThemeOptions } from "@mui/material";

export const darkStyles: ThemeOptions = {
	palette: {
		mode: "dark",
		primary: {
			main: "#FFFFFF",
		},
		secondary: {
			light: "#999999",
			main: "#444444",
		},
		info: {
			main: "#0066CC",
		},
		background: {
			default: "#2C2C2C",
		},
	},
};
