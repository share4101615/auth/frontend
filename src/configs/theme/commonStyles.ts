import { ThemeOptions } from "@mui/material";

export const commonStyles: ThemeOptions = {
	typography: {
		fontFamily: [
			"IBM Plex Sans",
			"Inter",
			"Roboto",
			'"Helvetica Neue"',
			"Arial",
			"sans-serif",
			'"Apple Color Emoji"',
			'"Segoe UI Emoji"',
			'"Segoe UI Symbol"',
		].join(","),
	},
	components: {
		MuiTypography: {
			styleOverrides: {
				h2: {
					fontSize: "2rem",
					fontWeight: 700,
					fontFamily: "IBM Plex Sans",
				},
				h5: {
					fontSize: "1.25rem",
					fontWeight: 600,
					fontFamily: "IBM Plex Sans",
					lineHeight: "1.5",
					letterSpacing: "-2%",
				},
				h6: {
					fontSize: "1rem",
					fontWeight: 600,
					fontFamily: "IBM Plex Sans",
					lineHeight: "1.5",
				},
			},
		},
		MuiTextField: {
			defaultProps: {
				variant: "outlined",
				InputLabelProps: {
					shrink: true,
				},
			},
		},
		MuiOutlinedInput: {
			styleOverrides: {
				root: {
					// Без этого параметра в хроме в форме логина появляется бордер перед кнопкой закругленный
					WebkitBorderRadius: 0,
				},
			},
		},
	},
};
